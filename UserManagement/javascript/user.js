//it Creates Heading to application
var userManagementHeading =  document.createElement("h2");
userManagementHeading.appendChild(document.createTextNode("User Management"));
userManagementHeading.setAttribute("align",'center');
userManagementHeading.setAttribute("id",'headingID');

createUser();

// its used to save the user and display their data
function  createUser()
{
var loadButton = document.createElement("button");
var loadButtonTextNode = document.createTextNode("Users Details");
loadButton.appendChild(loadButtonTextNode);
loadButton.setAttribute("align",'center');
loadButton.addEventListener('click',function()
{
   fetchUser();
});
var idTextNode = document.createTextNode('ID:');
var id =  document.createElement("input");
id.setAttribute("id",'id1');
id.setAttribute("type","number");
id.placeholder="Enter Id";


var name = document.createElement("input");
name.setAttribute("id",'name1');
name.placeholder="Enter name";
id.setAttribute("type","text");


var sapId =  document.createElement("input");
sapId.setAttribute("id",'sapId1');
sapId.placeholder="Enter sapId";
sapId.setAttribute("type","number");


var mobile = document.createElement("input");
mobile.setAttribute("type","number");
mobile.setAttribute("id",'mobile1');
mobile.placeholder="Enter mobile";


var email =  document.createElement("input");
email.setAttribute("id",'email1');
email.setAttribute("type","email");
email.placeholder="Enter email";

var gender = document.createElement("select");

var opt1 = document.createElement('option');
opt1.appendChild( document.createTextNode('Male') );
opt1.value = 'Male'; 
gender.appendChild(opt1);

var opt2 = document.createElement('option');
opt2.appendChild( document.createTextNode('Female') );
opt2.value = 'Female'; 
gender.appendChild(opt2);

gender.setAttribute("id",'gender1');
gender.placeholder="Enter gender";


var saveButton = document.createElement("button");
var saveButtonTextNode = document.createTextNode("Save User");
saveButton.appendChild(saveButtonTextNode);
saveButton.addEventListener('click',function()
{
   saveUser();
});

var idDiv=  document.createElement("div");
idDiv.appendChild(document.createTextNode("ID:"));


var nameDiv=  document.createElement("div");
nameDiv.appendChild(document.createTextNode("Name:"));

var sapDiv=  document.createElement("div");
sapDiv.appendChild(document.createTextNode("Sap ID:"));

var mobileDiv=  document.createElement("div");
mobileDiv.appendChild(document.createTextNode("Mobile:"));

var emailDiv=  document.createElement("div");
emailDiv.appendChild(document.createTextNode("Email:"));

var genderDiv=  document.createElement("div");
genderDiv.appendChild(document.createTextNode("Gender:"));
var emptyDiv=  document.createElement("div");

 
document.body.appendChild(userManagementHeading);
document.body.appendChild(loadButton);
document.body.appendChild(nameDiv);
document.body.appendChild(name);
document.body.appendChild(sapDiv);
document.body.appendChild(sapId);
document.body.appendChild(mobileDiv);
document.body.appendChild(mobile);
document.body.appendChild(emailDiv);
document.body.appendChild(email);
document.body.appendChild(genderDiv);
document.body.appendChild(gender);
document.body.appendChild(emptyDiv);
document.body.appendChild(saveButton);



}
//its used to fetch user details
function fetchUser()
{    
//its used to delete the table if already exist
var tableEl = document.getElementsByTagName('table');
if (tableEl[0] !== undefined) {
   tableEl[0].remove()
}
var httpReq;
if(window.XMLHttpRequest)
{
  httpReq = new XMLHttpRequest
}
else{
  httpReq  = new ActiveXObject();
}
httpReq.onreadystatechange = function()
{
  if(this.readyState===4 && this.status===200)
  {
      var table = document.createElement('table');
      table.setAttribute("id",'dataTable');
      
      var thead = document.createElement('thead');
      var tbody = document.createElement('tbody');

      var headTr = document.createElement('tr');

      var td1 = document.createElement('td');
      var td1Text = document.createTextNode("Id");
      td1.appendChild(td1Text);

      var td2 = document.createElement('td');
      var td2Text = document.createTextNode("Name");
      td2.appendChild(td2Text);

      var td3 = document.createElement('td');
      var td3Text = document.createTextNode("SapId");
      td3.appendChild(td3Text);

      var td4 = document.createElement('td');
      var td4Text = document.createTextNode("Mobile");
      td4.appendChild(td4Text);

      var td5 = document.createElement('td');
      var td5Text = document.createTextNode("Email");
      td5.appendChild(td5Text);

      var td6 = document.createElement('td');
      var td6Text = document.createTextNode("Gender");
      td6.appendChild(td6Text);

      var td7 = document.createElement('td');
      var td7Text = document.createTextNode("Action");
      td7.appendChild(td7Text);

      

      headTr.appendChild(td1);
      headTr.appendChild(td2);
      headTr.appendChild(td3);
      headTr.appendChild(td4);
      headTr.appendChild(td5);
      headTr.appendChild(td6);
      headTr.appendChild(td7);

      thead.appendChild(headTr);

       var data = JSON.parse(this.response);
       var len = data.length;
      if(len>0)
      {
       var removeTab = document.getElementById("dataTable");                   
       var body = document.getElementsByTagName('body')[0];
      
       for(var i=0;i<len;i++)
       {
           
           var tbodyTr = document.createElement("tr");
           var td1 = document.createElement("td");
           var td2 = document.createElement("td");
           var td3 = document.createElement("td");
           var td4 = document.createElement("td");
           var td5 = document.createElement("td");
           var td6 = document.createElement("td");
           var td7 = document.createElement("td");

           var td1TextNode = document.createTextNode(data[i].id);
           var td2TextNode = document.createTextNode(data[i].name);
           var td3TextNode = document.createTextNode(data[i].sapId);
           var td4TextNode = document.createTextNode(data[i].mobile);
           var td5TextNode = document.createTextNode(data[i].email);
           var td6TextNode = document.createTextNode(data[i].gender);

       var updateButton = document.createElement("button");
       var updateButtonTextNode = document.createTextNode("Update");
       updateButton.appendChild(updateButtonTextNode);
       updateButton.addEventListener('click',function()
       {
           var data = this.parentElement.parentElement.cells;
           console.log(data);
           var obj1 = {id:data[0].innerHTML,name:data[1].innerHTML,sapId:data[2].innerHTML,mobile:data[3].innerHTML,email:data[4].innerHTML,gender:data[5].innerHTML};
           localStorage.setItem('user',JSON.stringify(obj1));
           window.location.assign("userupdate.html");
           fetchUser();
       })

       var deleteButton = document.createElement("button");
       var deleteButtonTextNode = document.createTextNode("Delete");
       deleteButton.appendChild(deleteButtonTextNode);
       deleteButton.addEventListener('click',function()
       {
           deleteUser(this);
       });

           td1.appendChild(td1TextNode);
           td2.appendChild(td2TextNode);
           td3.appendChild(td3TextNode);
           td4.appendChild(td4TextNode);
           td5.appendChild(td5TextNode);
           td6.appendChild(td6TextNode);
           td7.appendChild(updateButton);
           td7.appendChild(deleteButton);
       

           tbodyTr.appendChild(td1);
           tbodyTr.appendChild(td2);
           tbodyTr.appendChild(td3);
           tbodyTr.appendChild(td4);
           tbodyTr.appendChild(td5);
           tbodyTr.appendChild(td6);
           tbodyTr.appendChild(td7);

           tbody.appendChild(tbodyTr);

       }
      }
      else{
            var data = document.createElement("h4");
            var noData = document.createTextNode("Data is Not Available");
            data.appendChild(noData);
            tbody.appendChild(data);
      }
       
      table.appendChild(thead);
      table.appendChild(tbody);

      var body = document.getElementsByTagName('body')[0];
      body.appendChild(table);
  }
}

httpReq.open('GET',"http://localhost:3000/users",true);
httpReq.send();
}
//it saves user details after performing validation of user
function saveUser()
{
var  name = document.getElementById("name1").value;
var  sapId = document.getElementById("sapId1").value;
var  mobile = document.getElementById("mobile1").value;
var  email = document.getElementById("email1").value;

var pattern="[A-Za-z0-9._%+-]*(@dbs.com|@hcl.com)";

if(name=="" || name==null){
alert("Please Enter Name");
return false;       
}

if(sapId ==null || sapId ==""){
alert("Please Enter SapId ");
return false;
}
if(mobile=="" || mobile==null){
alert("Please Enter Mobile");
return false;       
}
if(mobile.length!=10){
alert("Please Enter 10 digit Mobile Number");
return false;       
}
if(email=="" || email==null){
alert("Please Enter Email");
return false;       
}

if(!email.match(pattern)){
  alert("Please Enter valid Email");
  return false;       
  }

var httpReq;
if(window.XMLHttpRequest)
{
  httpReq = new XMLHttpRequest
}
else{
  httpReq  = new ActiveXObject();
}
httpReq.onreadystatechange = function()
{
   if(this.readyState===4 && this.status===201)
   {
           alert("User added successfully");
           fetchUser();
   }
}

var  name = document.getElementById("name1").value;
var  sapId = document.getElementById("sapId1").value;
var  mobile = document.getElementById("mobile1").value;
var  email = document.getElementById("email1").value;
var  gender = document.getElementById("gender1").value;

var obj = {name:name,sapId:sapId,mobile:mobile,email:email,gender:gender};

httpReq.open('POST',"http://localhost:3000/users",true);
httpReq.setRequestHeader("Content-type","application/json");
httpReq.send(JSON.stringify(obj));


}
//it deletes the user selected
function deleteUser(id)
{
var obj3 = confirm("Are you sure you want to delete?");
if (obj3==true)
{
   var httpReq;
   
   if(window.XMLHttpRequest)
   {
       httpReq = new XMLHttpRequest
   }
   else{
       httpReq  = new ActiveXObject();
   }

   httpReq.onreadystatechange = function()
   {
           if(this.readyState===4 && this.status===200)
           {
                   alert("Deleted User successfully");
                   fetchUser();
           }
           
   }

   var data = id.parentElement.parentElement.cells;
   console.log("data"+data);
           var del = data[0].innerHTML;

   httpReq.open('DELETE',"http://localhost:3000/users/"+del,true);
   httpReq.send();
}
else
    return false;

}