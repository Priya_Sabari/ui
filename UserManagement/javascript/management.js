//this is the heading
var updateHeading =  document.createElement("h2");
updateHeading.appendChild(document.createTextNode("User Details Update"));
updateHeading.setAttribute("align",'center');
updateHeading.setAttribute("id",'headingID');

var id =  document.createElement("input");
id.setAttribute("id",'id');
id.setAttribute("readOnly",true);
id.addEventListener('mouseover',function()
{
   alert("Please Fill the data to Update records of User");
});

var name1 = document.createElement("input");
name1.setAttribute("type","text");
name1.setAttribute("id",'name');

var sapId =  document.createElement("input");
sapId.setAttribute("type","number");
sapId.setAttribute("id",'sapId');

var mobile = document.createElement("input");
mobile.setAttribute("type","number");
mobile.setAttribute("id",'mobile');

var email =  document.createElement("input");
email.setAttribute("id",'email');

var gender = document.createElement("select");

var opt1 = document.createElement('option');
opt1.appendChild( document.createTextNode('Male') );
opt1.value = 'Male'; 
gender.appendChild(opt1);

var opt2 = document.createElement('option');
opt2.appendChild( document.createTextNode('Female') );
opt2.value = 'Female'; 
gender.appendChild(opt2);

gender.setAttribute("id",'gender');

var updateButton = document.createElement("button");
var updateButtonTextNode = document.createTextNode("Update");
updateButton.appendChild(updateButtonTextNode);
updateButton.addEventListener('click',function()
{
   updateUser();
});

var cancelButton = document.createElement("button");
var cancelButtonTextNode = document.createTextNode("Cancel");
cancelButton.appendChild(cancelButtonTextNode);
cancelButton.addEventListener('click',function()
{
   history.go(-1);
});

var idDiv=  document.createElement("div");
idDiv.appendChild(document.createTextNode("ID:"));


var nameDiv=  document.createElement("div");
nameDiv.appendChild(document.createTextNode("Name:"));

var sapDiv=  document.createElement("div");
sapDiv.appendChild(document.createTextNode("Sap ID:"));

var mobileDiv=  document.createElement("div");
mobileDiv.appendChild(document.createTextNode("Mobile:"));

var emailDiv=  document.createElement("div");
emailDiv.appendChild(document.createTextNode("Email:"));

var genderDiv=  document.createElement("div");
genderDiv.appendChild(document.createTextNode("Gender:"));
var emptyDiv=  document.createElement("div");

var emptyDiv=  document.createElement("div");

var body = document.getElementsByTagName('body')[0];

document.body.appendChild(updateHeading);
document.body.appendChild(idDiv);
body.appendChild(id);
document.body.appendChild(nameDiv);
body.appendChild(name1);
document.body.appendChild(sapDiv);
body.appendChild(sapId);
document.body.appendChild(mobileDiv);
body.appendChild(mobile);
document.body.appendChild(emailDiv);
body.appendChild(email);
document.body.appendChild(genderDiv);
body.appendChild(gender);
body.appendChild(emptyDiv);
body.appendChild(cancelButton);
body.appendChild(updateButton);

//From localstorage it fetch the user data and displays it
var data = JSON.parse(localStorage.getItem("user"));
console.log(data);
document.getElementById("id").value = data.id;
document.getElementById("name").value = data.name;
document.getElementById("sapId").value = data.sapId;
document.getElementById("mobile").value = data.mobile;
document.getElementById("email").value = data.email;
document.getElementById("gender").value = data.gender;


//this method updated user data
function updateUser() {
var id = document.getElementById("id").value;
var name = document.getElementById("name").value;
var sapId = document.getElementById("sapId").value;
var mobile = document.getElementById("mobile").value;            
var email = document.getElementById("email").value;
var gender = document.getElementById("gender").value;

if(id=="" || id==null){
alert("Please Enter Id");
return false;       
}
if(name=="" || name==null){
   alert("Please Enter Name");
   return false;       
}

if(sapId ==null || sapId ==""){
   alert("Please Enter SapId ");
   return false;
}
if(mobile=="" || mobile==null){
   alert("Please Enter Mobile");
   return false;       
}
if(mobile.length!=10){
   alert("Please Enter 10 digit Mobile Number");
   return false;       
}
if(email=="" || email==null){
   alert("Please Enter Email");
   return false;       
}


var obj = { id:id,name: name, sapId: sapId,mobile: mobile, email: email, gender: gender };

var httpRequest;
if (window.XMLHttpRequest) {
   httpRequest = new XMLHttpRequest()
} else {
   httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
}
httpRequest.onreadystatechange = function () {
   if (this.readyState === 4 && this.status === 200) {
       alert("User updated successfully");
       console.log(this.response);
       history.back();

   }
}
httpRequest.open("put", "http://localhost:3000/users/" + data.id, true);
httpRequest.setRequestHeader("Content-type", "application/json");
httpRequest.send(JSON.stringify(obj));

}
